FROM php:7.4-alpine

RUN apk add --no-cache wget unzip nano curl sudo mysql mysql-client bash git python3 rsync bash bash-completion duplicity lftp

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions gd xdebug intl zip mysqli curl dom fileinfo json mbstring openssl pdo_mysql tokenizer

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash && \
    apk add symfony-cli

RUN adduser -D -s /bin/bash app
RUN echo "app  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/app

USER app

WORKDIR /app
