<?php

namespace App\Command;

use App\Traits\CommandLoggerTrait;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;

class ReportCommand extends Command
{
    use CommandLoggerTrait;
    use LockableTrait;

    private const ERROR_LEVELS = [LogLevel::CRITICAL, LogLevel::ERROR];
    private const TITLE = '------------ %s ------------';
    private const BREAK_LINE = "\n";

    protected static $defaultName = 'app:backup:report';
    protected static $defaultDescription = 'Send a report by email';

    private Filesystem $filesystem;
    private NotifierInterface $notifier;
    private LoggerInterface $logger;
    private ContainerInterface $parameters;
    private string $projectDir;
    private string $adminEmail;

    private array $db = [];
    private array $ftp = [];
    private array $clean = [];
    private string $raw = '';
    private bool $hasError = false;
    private string $message = '';
    private array $titles;

    public function __construct(
        Filesystem            $filesystem,
        NotifierInterface     $notifier,
        LoggerInterface       $commandLogger,
        string $projectDir,
        string $adminEmail,
        string $name = null
    ) {
        $this->filesystem = $filesystem;
        $this->notifier = $notifier;
        $this->logger = $commandLogger;
        $this->projectDir = $projectDir;
        $this->adminEmail = $adminEmail;

        $this->titles = [
            BackupDatabaseCommand::getDefaultName() => 'Backup databases',
            FTPCommand::getDefaultName() => 'FTP backup',
            CleanUpCommand::getDefaultName() => 'Clean up',
        ];

        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $this->log('warning', 'ReportCommand is already running in another process.');

            return Command::SUCCESS;
        }

        $this->log('debug', 'ReportCommand starts');
        try {
            $this->fetchReport();
            if ($this->hasError) {
                $this->formatReport($this->db);
                $this->formatReport($this->ftp);
                $this->formatReport($this->clean);
            }
            $this->process();
        } catch (\Throwable $exception) {
            $this->log('critical', 'ReportCommand failed', ['exception' => $exception]);
            $this->release();

            return Command::FAILURE;
        }

        $this->log('debug', 'ReportCommand ended');

        $this->release();

        return Command::SUCCESS;
    }

    private function fetchReport(): void
    {
        $date = (new \DateTime())->format('Y-m-d');
        $pathname = $this->projectDir . '/var/log/prod-' . $date . '.log';
        if (!$this->filesystem->exists($pathname)) {
            return;
        }

        $this->raw = file_get_contents($pathname);
        $rows = explode("\n", $this->raw);

        foreach ($rows as $row) {
            $report = json_decode($row);
            switch ($report->context->command) {
                case BackupDatabaseCommand::getDefaultName():
                    $this->db[] = $report;
                    break;
                case FTPCommand::getDefaultName():
                    $this->ftp[] = $report;
                    break;
                case CleanUpCommand::getDefaultName():
                    $this->clean[] = $report;
                    break;
            }

            if (!$this->hasError) {
                $this->hasError = in_array(strtolower($report->level_name), self::ERROR_LEVELS);
            }
        }
    }

    private function process(): void
    {
        $subject = 'DC-Trad Backup 🔵';
        $content = $this->message;
        $importance = Notification::IMPORTANCE_LOW;

        if ($this->hasError) {
            $subject = 'DC-Trad Backup 🔴';
            $content .= self::BREAK_LINE . self::BREAK_LINE . $this->raw;
            $importance = Notification::IMPORTANCE_HIGH;
        }

        $notification = (new Notification($subject))
            ->content($content)
            ->importance($importance);

        $recipient = new Recipient($this->adminEmail);

        $this->notifier->send($notification, $recipient);
    }

    private function formatReport(array $rows)
    {
        $this->message .= sprintf(self::TITLE, $this->titles[$rows[0]->context->command]);
        $this->message .= self::BREAK_LINE;
        foreach ($rows as $row) {
            $this->message .= $row->message;
            $this->message .= self::BREAK_LINE;
        }
    }
}
