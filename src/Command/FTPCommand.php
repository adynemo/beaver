<?php

namespace App\Command;

use App\Traits\CommandLoggerTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class FTPCommand extends Command
{
    use CommandLoggerTrait;
    use LockableTrait;

    private const DEFAULT_TIMEOUT = 300;
    private const DEFAULT_FULL_IF_OLDER_THAN = '1M';
    private const DEFAULT_REMOVE_OLDER_THAN = '2M';

    protected static $defaultName = 'app:backup:ftp';
    protected static $defaultDescription = 'Send backup files to a FTP';

    private Filesystem $filesystem;
    private LoggerInterface $logger;
    private array $config;
    private int $timeout = self::DEFAULT_TIMEOUT;

    public function __construct(Filesystem $filesystem, LoggerInterface $commandLogger, array $config, string $name = null)
    {
        $this->filesystem = $filesystem;
        $this->config = $config;
        $this->logger = $commandLogger;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addOption('databases',  'd', InputOption::VALUE_NONE, 'To backup only dumps directory')
            ->addOption('sites', 's', InputOption::VALUE_NONE, 'To backup only sites directory')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $this->log('warning', 'FTPCommand is already running in another process.');

            return Command::SUCCESS;
        }

        $this->log('debug', 'FTPCommand starts');

        $isDatabases = $input->getOption('databases');
        $isSites = $input->getOption('sites');
        $isFull = $isDatabases && $isSites;

        if (!($isDatabases || $isSites || $isFull)) {
            $output->writeln('You have to pass at least one option');
            $this->release();

            return Command::SUCCESS;
        }

        try {
            if ($isFull || $isDatabases) {
                $this->process(true);
            }
            if ($isFull || $isSites) {
                $this->process(false);
            }
        } catch (\Throwable $exception) {
            $this->log('critical', 'CleanUpCommand failed', ['exception' => $exception]);
            $this->release();

            return Command::FAILURE;
        }

        $this->log('debug', 'FTPCommand ended');

        $this->release();

        return Command::SUCCESS;
    }

    private function process(bool $isDatabase): void
    {
        $archiveDir = $isDatabase ? $this->config['databases_dir'] : $this->config['sites_dir'];
        if (!$this->filesystem->exists($archiveDir)) {
            $this->log('debug',
                'This directory does not exist. No backup will be processed',
                ['directory' => $archiveDir]
            );
            return;
        }
        $this->timeout = $this->config['timeout'] ?? self::DEFAULT_TIMEOUT;

        $ftpCommand = sprintf(
            'ftp://%1$s:%2$s@%3$s%4$s',
            $this->config['user'],
            $this->config['password'],
            $this->config['host'],
            $isDatabase ? $this->config['ftp_databases_dir'] : $this->config['ftp_sites_dir'],
        );

        $command = [
            'duplicity',
            '--no-encryption',
            '--full-if-older-than',
            $this->config['full_if_older_than'] ?? self::DEFAULT_FULL_IF_OLDER_THAN,
        ];

        $exclusion = !$isDatabase ? $this->config['exclusion_filename'] : null;
        if (null !== $exclusion && $this->filesystem->exists($exclusion)) {
            $command[] = '--exclude-filelist';
            $command[] = $exclusion;
        }

        $command[] = $archiveDir;
        $command[] = $ftpCommand;

        $this->log('debug', 'Backup database dump directory');
        $this->runProcess($command, function ($data) {
            return $this->strContains("/Backup Statistics/", $data) &&
                !$this->strContains("/Errors 0/", $data);
        });

        $command = [
            'duplicity',
            'verify',
            '--no-encryption',
            $ftpCommand,
            $archiveDir,
        ];
        $this->log('debug', 'Check backup database dump directory');
        $this->runProcess($command, function ($data) {
            return $this->strContains("/Verify complete/", $data) &&
                !$this->strContains("/, 0 differences found/", $data);
        });

        if (true === $this->config['remove_older']) {
            $olderThan = $this->config['remove_older_than'] ?? self::DEFAULT_FULL_IF_OLDER_THAN;
            $this->log('debug', 'Remove older backup');
            $command = [
                'duplicity',
                'remove-older-than',
                $olderThan,
                '--force',
                '--no-encryption',
                $ftpCommand,
                $archiveDir,
            ];
            $this->runProcess($command, function ($data) {
                return false;
            });
        }

        $this->log('debug', 'Backup ended');
    }

    private function runProcess(array $command, callable $errorCondition): void
    {
        $process = new Process($command);
        $process->setTimeout($this->timeout);
        $process->start();
        $this->logOutputs($process, $errorCondition);
        $process->wait();
    }

    private function logOutputs(Process $process, callable $errorCondition): void
    {
        foreach ($process as $type => $data) {
            $level = 'error';
            if ($process::OUT === $type) {
                $level = 'info';
                if ($errorCondition($data)) {
                    $level = 'error';
                } else if ($this->strContains("/exception|error/i", $data)) {
                    $level = 'warning';
                }
            }
            $this->log($level, $data);
        }
    }

    private function strContains(string $pattern, string $data): bool
    {
        return 1 === preg_match($pattern, $data);
    }
}
