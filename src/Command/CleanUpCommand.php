<?php

namespace App\Command;

use App\Traits\CommandLoggerTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class CleanUpCommand extends Command
{
    use CommandLoggerTrait;
    use LockableTrait;

    private const DEFAULT_MAX_FILES = 30;

    protected static $defaultName = 'app:backup:clean-up';
    protected static $defaultDescription = 'Remove old databases backup files';

    private Filesystem $filesystem;
    private LoggerInterface $logger;
    private array $projects;

    public function __construct(Filesystem $filesystem, LoggerInterface $commandLogger, array $projects, string $name = null)
    {
        $this->filesystem = $filesystem;
        $this->projects = $projects;
        $this->logger = $commandLogger;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('project', InputArgument::OPTIONAL, 'Project key to process only this one')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $this->log('warning', 'CleanUpCommand is already running in another process.');

            return Command::SUCCESS;
        }

        $this->log('debug', 'CleanUpCommand starts');

        $projectKey = $input->getArgument('project');
        if (null !== $projectKey && !isset($this->projects[$projectKey])) {
            $this->log('warning', 'No project found with this key', ['argument' => $projectKey]);
            $this->release();

            return Command::FAILURE;
        }

        try {
            null !== $projectKey ? $this->processOne($this->projects[$projectKey]) : $this->processAll();
        } catch (\Throwable $exception) {
            $this->log('critical', 'CleanUpCommand failed', ['exception' => $exception]);
            $this->release();

            return Command::FAILURE;
        }

        $this->log('debug', 'CleanUpCommand ended');

        $this->release();

        return Command::SUCCESS;
    }

    private function processOne(array $project): void
    {
        $this->log('debug', 'Process project', ['project' => $project['name']]);
        $dumpDir = $project['dump_dir'];
        if (!$this->filesystem->exists($dumpDir)) {
            $this->log('warning', 'Dump directory does not exist', ['directory' => $dumpDir]);
            return;
        }

        $maxFiles = $project['max_files'] ?? self::DEFAULT_MAX_FILES;

        $finder = new Finder();
        $finder->files()->sortByName()->in($dumpDir);
        if (!$finder->hasResults() || $maxFiles >= $finder->count()) {
            $this->log('debug', 'No dump file to remove');
            return;
        }

        $this->log('debug', 'Dump files will be removed', ['count' => $finder->count() - $maxFiles]);

        $files = (array) $finder->getIterator();
        $deletion = 0;
        while ($maxFiles < count($files)) {
            $file = array_shift($files);
            $this->log('debug', 'Removes dump file', ['pathname' => $file->getPathname()]);
            $this->filesystem->remove($file->getPathname());
            $deletion++;
        }

        $this->log('debug', 'Dump files have been removed', [
            'project' => $project['name'],
            'count' => $deletion
        ]);
    }

    private function processAll(): void
    {
        foreach ($this->projects as $project)
        {
            $this->processOne($project);
        }
    }
}
