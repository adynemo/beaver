<?php

namespace App\Command;

use App\Traits\CommandLoggerTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class BackupDatabaseCommand extends Command
{
    use CommandLoggerTrait;
    use LockableTrait;

    protected static $defaultName = 'app:backup:db';
    protected static $defaultDescription = 'Process databases backup for the configured projects';

    private Filesystem $filesystem;
    private LoggerInterface $logger;
    private array $projects;

    public function __construct(Filesystem $filesystem, LoggerInterface $commandLogger, array $projects, string $name = null)
    {
        $this->filesystem = $filesystem;
        $this->projects = $projects;
        $this->logger = $commandLogger;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('project', InputArgument::OPTIONAL, 'Project key to process only this one')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $this->log('warning', 'BackupDatabaseCommand is already running in another process.');

            return Command::SUCCESS;
        }

        $this->log('debug', 'BackupDatabaseCommand starts');

        $projectKey = $input->getArgument('project');
        if (null !== $projectKey && !isset($this->projects[$projectKey])) {
            $this->log('warning', 'No project found with this key', ['argument' => $projectKey]);
            $this->release();

            return Command::FAILURE;
        }

        try {
            null !== $projectKey ? $this->processOne($this->projects[$projectKey]) : $this->processAll();
        } catch (\Throwable $exception) {
            $this->log('critical', 'BackupDatabaseCommand failed', ['exception' => $exception]);
            $this->release();

            return Command::FAILURE;
        }

        $this->log('debug', 'BackupDatabaseCommand ended');

        $this->release();

        return Command::SUCCESS;
    }

    private function processOne(array $project): void
    {
        $this->log('debug', 'Backup project', ['project' => $project['name']]);
        $dumpDir = $project['dump_dir'];
        if (!$this->filesystem->exists($dumpDir)) {
            $this->log('debug', 'Dump directory does not exist. It will be created', ['directory' => $dumpDir]);
            $this->filesystem->mkdir($dumpDir, 0700);
        }

        $this->log('debug', 'Dump database');

        $date = (new \DateTimeImmutable())->format('Y-m-d');
        $filename = sprintf('%1$s/dump-%2$s-%3$s.sql', $dumpDir, strtolower($project['name']), $date);
        $databaseConfig = $project['database'];

        $command = [
            'mysqldump',
            '-h',
            $databaseConfig['host'],
            '-u',
            $databaseConfig['user'],
            '-p'.$databaseConfig['password'],
            $databaseConfig['name'],
        ];

        $process = new Process($command);
        $process->run();

        $error = $process->getErrorOutput();
        if ('' !== $error) {
            $this->log('error', 'Unable to process backup', [
                'project' => $project['name'],
                'error' => $error
            ]);

            throw new ProcessFailedException($process);
        }

        $dump = $process->getOutput();
        $this->filesystem->dumpFile($filename, $dump);

        $this->log('debug', 'Backup ended', [
            'project' => $project['name'],
            'filename' => $filename
        ]);
    }

    private function processAll(): void
    {
        foreach ($this->projects as $project)
        {
            $this->processOne($project);
        }
    }
}
