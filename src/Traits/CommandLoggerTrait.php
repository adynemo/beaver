<?php

namespace App\Traits;

trait CommandLoggerTrait
{
    private function log(string $level, string $message, array $context = []): void
    {
        $context = array_merge(['command' => self::$defaultName], $context);
        $this->logger->$level($message, $context);
    }
}
